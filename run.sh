#!/bin/bash

docker run --rm \
           -it \
           --name meme2vec-generator \
           -v ${PWD}/frontend:/sources \
           -v ${PWD}:/project \
           -p 80:80 \
           meme2vec:generator
