import os

import flask

import meme2vec as m2v


app = flask.Flask(__name__, static_folder='frontend')
generator = m2v.generator.Generator(
    word_mappings_file='data/word_embeddings-stops.npy',
    grammar_mappings_file='data/grammar-mappings.pickle',
    nn_model_file='data/model.h5'
)


@app.route('/')
def main():
    return app.send_static_file('index.html')


@app.route('/js/<path:filename>')
def serve_js(filename):
    return app.send_static_file(os.path.join('js', filename))


@app.route('/css/<path:filename>')
def serve_css(filename):
    return app.send_static_file(os.path.join('css', filename))


@app.route('/templates/<path:filename>')
def server_templates(filename):
    return flask.send_from_directory('data/templates', filename)


@app.route('/meme-template', methods=['POST'])
def get_template():
    data = flask.request.get_json(silent=True)
    response = {
        'template': generator.predict_template(data['text']),
    }

    return flask.jsonify(response)


if __name__ == '__main__':
    app.run(debug=True)

